
DESCRIPTION

This module works with uc_node_checkout and scheduler to enable the purchase of nodes with a pre-configured time duration which is useful for paid listings such as jobs and classifieds.


INSTALLATION

Install uc_node_checkout_scheduler, uc_node_checkout and scheduler modules

Node Checkout
Set up your content type that you want people to be able to purchase, making sure that you set the Workflow settings to unpublished at admin/content/types
Create the product that you want to use to enable the purchase and any attributes you require
Map the node to the product: admin/store/settings/node-checkout/overview

Scheduler
Set a content type on for scheduler start and finish dates in admin/content/types
Since the start and expire times are set via a conditional action then only allow admin access to the scheduler details so that users cannot edit their unpublish date

Node Checkout Scheduler
Edit a content type that has been enabled for scheduler and there should be a tab 'Node Scheduler' (admin/content/types).  Here you set the start and expire times for the content type, for each attribute if the product has any.
A conditional action will apply these values to the node once payment is received
Notifications - these can be set in the conditional actions supplied
Renew - once a node has expired it can be renewed again with a click of a link sent out in an email


