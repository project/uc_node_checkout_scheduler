<?php // $Id$
/**
 * @file
 * This file contains the Conditional Actions hooks and functions necessary to make the
 * nodes-related entity, conditions, events, and actions work.
 */


/******************************************************************************
 * Conditional Actions Hooks                                                  *
 ******************************************************************************/

/**
 * Implementation of hook_ca_predicate().
 */
function uc_node_checkout_scheduler_ca_predicate() {
  // Set the node status to published when a payment is received
  // and the balance is less than or equal to 0.
  $configurations['uc_node_checkout_scheduler_received'] = array(
    '#title' => t('Publish node on full payment'),
    '#description' => t('Only happens when a payment is entered and the balance is <= $0.00.'),
    '#class' => 'payment',
    '#trigger' => 'uc_payment_entered',
    '#status' => 1,
    '#conditions' => array(
      '#operator' => 'AND',
      '#conditions' => array(
        array(
          '#name' => 'uc_payment_condition_order_balance',
          '#title' => t('If the balance is less than or equal to $0.00.'),
          '#argument_map' => array(
            'order' => 'order',
          ),
          '#settings' => array(
            'negate' => FALSE,
            'balance_comparison' => 'less_equal',
          ),
        ),
      ),
    ),
    '#actions' => array(
      array(
        '#name' => 'uc_node_checkout_scheduler_update',
        '#title' => t('Update the node status to Published.'),
        '#argument_map' => array(
          'order' => 'order',
        ),
      ),
    ),
  );


  $args = array(
    'account' => 'account',
    'node' => 'node',
  );

  // Notify the user when node is about to be unpublished
  $configurations['uc_node_checkout_scheduler_notify_reminder'] = array(
    '#title' => t('Node Unpublish Reminder'),
    '#description' => t('Notify the customer when they have a node that is about to be unpublished.'),
    '#class' => 'notification',
    '#trigger' => 'uc_node_checkout_scheduler_notify_reminder',
    '#status' => 1,
    '#actions' => array(
      array(
        '#name' => 'uc_node_checkout_scheduler_reminder_email',
        '#title' => t('Send an e-mail to the customer'),
        '#argument_map' => $args,
        '#settings' => array(
          'from' => uc_store_email_from(),
          'addresses' => '[mail]',
          'subject' => uc_get_message('uc_node_checkout_scheduler_reminder_subject'),
          'message' => uc_get_message('uc_node_checkout_scheduler_reminder_message'),
          'format' => 1,
        ),
      ),
    ),
  );

  // Notify the user when a node is unpublished.
  $configurations['uc_node_checkout_scheduler_notify_unpublish'] = array(
    '#title' => t('Notify that node is unpublished'),
    '#description' => t('Notify the customer when they have had a node unpublished.'),
    '#class' => 'notification',
    '#trigger' => 'uc_node_checkout_scheduler_notify_unpublish',
    '#status' => 1,
    '#actions' => array(
      array(
        '#name' => 'uc_node_checkout_scheduler_unpublish_email',
        '#title' => t('Send an e-mail to the customer'),
        '#argument_map' => $args,
        '#settings' => array(
          'from' => uc_store_email_from(),
          'addresses' => '[mail]',
          'subject' => uc_get_message('uc_node_checkout_scheduler_unpublish_subject'),
          'message' => uc_get_message('uc_node_checkout_scheduler_unpublish_message'),
          'format' => 1,
        ),
      ),
    ),
  );

  return $configurations;
}

/**
 * Implementation of hook_ca_action().
 */
function uc_node_checkout_scheduler_ca_action() {
  $order_arg = array(
    '#entity' => 'order',
    '#title' => t('Order'),
  );
  $node_arg = array(
    '#entity' => 'node',
    '#title' => t('Node'),
  );
  $user_arg = array(
    '#entity' => 'user',
    '#title' => t('User'),
  );
  $actions['uc_node_checkout_scheduler_update'] = array(
    '#title' => t('Set Node Publish and Unpublish Dates'),
    '#category' => t('Node Checkout'),
    '#callback' => 'uc_node_checkout_scheduler_update',
    '#arguments' => array(
      'order' => $order_arg,
    ),
  );

  // Renew a node unpublish.
  $actions['uc_node_checkout_scheduler_order_renew'] = array(
    '#title' => t('Renew a node on payment.'),
    '#category' => t('Node Checkout'),
    '#callback' => 'uc_node_checkout_scheduler_action_node_renew',
    '#arguments' => array(
      'account' => $user_arg,
      'node' => $node_arg,
    ),
  );

  // Send an email to an order with a node unpublish
  $actions['uc_node_checkout_scheduler_reminder_email'] = array(
    '#title' => t('Send an email notifying of impending node unpublish.'),
    '#category' => t('Node Checkout'),
    '#callback' => 'uc_node_checkout_scheduler_action_reminder_email',
    '#arguments' => array(
      'account' => $user_arg,
      'node' => $node_arg,
    ),
  );

  // Send an email to a user with a node unpublish
  $actions['uc_node_checkout_scheduler_unpublish_email'] = array(
    '#title' => t('Send an email notifying of node unpublish.'),
    '#category' => t('Node Checkout'),
    '#callback' => 'uc_node_checkout_scheduler_action_unpublish_email',
    '#arguments' => array(
      'account' => $user_arg,
      'node' => $node_arg,
    ),
  );

  return $actions;
}

/**
 * Implementation of hook_ca_trigger().
 */
function uc_node_checkout_scheduler_ca_trigger() {

  $node = array(
    '#entity' => 'node',
    '#title' => t('Node'),
  );
  $account = array(
    '#entity' => 'user',
    '#title' => t('User'),
  );

  $triggers['uc_node_checkout_scheduler_notify_reminder'] = array(
    '#title' => t('Node has almost reached its expiry date'),
    '#category' => t('Node Checkout'),
    '#arguments' => array(
      'user' => $account,
      'node' => $node,
    ),
  );

  $triggers['uc_node_checkout_scheduler_notify_unpublish'] = array(
    '#title' => t('Node has been unpublished'),
    '#category' => t('Node Checkout'),
    '#arguments' => array(
      'user' => $account,
      'node' => $node,
    ),
  );

  return $triggers;
}

/**
 * Send an email with order and node replacement tokens.
 *
 * The recipients, subject, and message fields take order token replacements.
 */
function uc_node_checkout_scheduler_action_reminder_email($account, $node, $settings) {
  $language = language_default();
  // Token replacements for the subject and body
  $settings['replacements'] = array(
    'global' => NULL,
    'node' => $node,
    'user' => $account,
  );

  // Replace tokens and parse recipients.
  $recipients = array();
  $addresses = token_replace_multiple($settings['addresses'], $settings['replacements']);
  foreach (explode("\n", $addresses) as $address) {
    $recipients[] = trim($address);
  }
  
  // Send to each recipient.
  foreach ($recipients as $email) {
    $sent = drupal_mail('uc_order', 'action-mail', $email, $language, $settings, $settings['from']);

    if (!$sent['result']) {
      watchdog('ca', 'Attempt to e-mail @email concerning order @order_id failed.', array('@email' => $email, '@order_id' => $order->order_id), WATCHDOG_ERROR);
    }
  }
}

// Email settings form
function uc_node_checkout_scheduler_action_reminder_email_form($form_state, $settings = array()) {
  return ca_build_email_form($form_state, $settings, array('global', 'user', 'node'));
}

/**
 * Send an email with order and node replacement tokens.
 *
 * The recipients, subject, and message fields take order token replacements.
 */
function uc_node_checkout_scheduler_action_unpublish_email($account, $node, $settings) {
  $language = language_default();

  // Token replacements for the subject and body
  $settings['replacements'] = array(
    'global' => NULL,
    'node' => $node,
    'user' => $account,
  );

  // Replace tokens and parse recipients.
  $recipients = array();
  $addresses = token_replace_multiple($settings['addresses'], $settings['replacements']);
  foreach (explode("\n", $addresses) as $address) {
    $recipients[] = trim($address);
  }

  // Send to each recipient.
  foreach ($recipients as $email) {
    $sent = drupal_mail('uc_order', 'action-mail', $email, $language, $settings, $settings['from']);

    if (!$sent['result']) {
      watchdog('ca', 'Attempt to e-mail @email concerning order @order_id failed.', array('@email' => $email, '@order_id' => $order->order_id), WATCHDOG_ERROR);
    }
  }
}

// Email settings form
function uc_node_checkout_scheduler_action_unpublish_email_form($form_state, $settings = array()) {
  return ca_build_email_form($form_state, $settings, array('global', 'user', 'node'));
}

/**
 * Builds an email settings form.
 */
if(!module_exists('uc_roles')) {
function ca_build_email_form($form_state, $settings, $token_filters) {
  $form = array();

  $form['from'] = array(
    '#type' => 'textfield',
    '#title' => t('Sender'),
    '#default_value' => $settings['from'],
    '#description' => t('The "From" address.'),
    '#required' => TRUE,
  );
  $form['addresses'] = array(
    '#type' => 'textarea',
    '#title' => t('Recipients'),
    '#default_value' => $settings['addresses'],
    '#description' => t('Enter the email addresses to receive the notifications, one on each line. You may use order tokens for dynamic email addresses.'),
    '#required' => TRUE,
  );
  $form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => $settings['subject'],
    '#required' => TRUE,
  );
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => $settings['message'],
  );

  // We add the #is_format element to allow us to locate and configure the filters.
  $form['format'] = filter_form($settings['format']) + array('#is_format' => TRUE);

  $form['token_help'] = array(
    '#type' => 'fieldset',
    '#title' => t('Replacement patterns'),
    '#description' => t('You can make use of the replacement patterns in the recipients field, the subject, and the message body.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  foreach ($token_filters as $name) {
    $form['token_help'][$name] = array(
      '#type' => 'fieldset',
      '#title' => t('@name replacement patterns', array('@name' => drupal_ucfirst($name))),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['token_help'][$name]['content'] = array(
      '#value' => theme('token_help', $name),
    );
  }

  return $form;
}
}
function uc_node_checkout_scheduler_update($order) {
  //  We publish each node and set a start and expire date for each order product
  foreach($order->products as $product) {    
	$node = node_load($product->data['node_checkout_nid']);	
	if($node->unpublish_on > time()) $renew = TRUE;
	foreach($product->data['attribute_ids'] as $attribute_id => $option_id) {
	  if($settings = uc_node_checkout_scheduler_get_settings($attribute_id, $option_id, $node->type)) {
	    if($settings['start_units']) {
		  if($renew) $start = _uc_node_checkout_scheduler_get_future_date($settings['start_units'], $settings['start_period'], $node->unpublish_on);
	      else $start = _uc_node_checkout_scheduler_get_future_date($settings['start_units'], $settings['start_period']);
	    }
	    else {
		  if($renew) $start = $node->unpublish_on;
		  else {
		    $start = time();
		    //  Publish the node immediately
		    $node->status = 1;
		  }
		}
	    if($settings['expire_units']) {
	      $expire = _uc_node_checkout_scheduler_get_future_date($settings['expire_units'], $settings['expire_period'], $start);
	    }
	    else $expire = '';
	    $node->publish_on = $start;
	    $node->unpublish_on = $expire;
		
		node_save($node);
		break;  // We have some values for publish and unpublish so we should get out now.
	  }
	}
  }
}
/**
 * Function will return a future time stamp given a certain amount of time
 * from a starting point (defaults to current time)
 *
 * @param $duration
 *   The amount of time until end date
 * @param $granularity
 *   A string representing the granularity's name (e.g. "day", "month", etc.)
 * @param $start_time
 *   The starting date for when the role will last
 * @return:
 *   A UNIX timestamp representing the second that end date takes place
 */
function _uc_node_checkout_scheduler_get_future_date($duration, $granularity, $start_time = NULL) {
  // No duration?
  if ($granularity == 'never') {
    return NULL;
  }

  $start_time = (!is_null($start_time)) ? $start_time : time();
  $operator = ($duration < 0) ? '' : '+';
  return strtotime($operator . $duration .' '. $granularity, $start_time);
}
